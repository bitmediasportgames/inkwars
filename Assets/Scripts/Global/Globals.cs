﻿namespace Argentics.InkWars.Global
{
    public static class Globals 
    {
        public class Constants
        {
            public class Tags
            {
                public const string PaintableTag = "Paintable";
                public const string StickmanTag = "Stickman";
                public const string InkGeneratorTag = "InkGenerator";
                public const string CannonTag = "Cannon";
            }

            public class BattleParameters
            {
                public const float DefaultActorInkAmount = 100;
                public const float DefaultInkRestoringSpeed = 1f;
                public const float RestoringTimePeriod = 1f;
                public const float InkGeneratorCaptureTime = 2f;
            }
        }
    }
}
