﻿namespace Argentics.InkWars.Data
{
    public class DataNotFoundException : System.Exception
    {
        public DataNotFoundException() : base() { }

        public DataNotFoundException(string message) : base(message) { }

        public DataNotFoundException(string message, System.Exception innerException) : base(message, innerException) { }
    }
}