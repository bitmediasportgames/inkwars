using System;
using System.Security.Cryptography;
using System.Text;

namespace Argentics.InkWars.Data.PlayerPrefsExtensions
{
    public static class SimpleEncryption
    {
        // IMPORTANT: This key must be exactly 32 characters long (256 bit).
        private static string key = ":{j%6j?E:t#}G10mM%9hp5S=%}2YYYYY";

        private static RijndaelManaged provider = null;

        private static void SetupProvider()
        {
            provider = new RijndaelManaged();
            provider.Key = Encoding.ASCII.GetBytes(key);
            provider.Mode = CipherMode.ECB;
        }

        public static string EncryptString(string sourceString)
        {
            if (provider == null)
            {
                SetupProvider();
            }
            ICryptoTransform encryptor = provider.CreateEncryptor();
            byte[] sourceBytes = Encoding.UTF8.GetBytes(sourceString);
            byte[] outputBytes = encryptor.TransformFinalBlock(sourceBytes, 0, sourceBytes.Length);
            return Convert.ToBase64String(outputBytes);
        }

        public static string DecryptString(string sourceString)
        {
            if (provider == null)
            {
                SetupProvider();
            }
            ICryptoTransform decryptor = provider.CreateDecryptor();
            byte[] sourceBytes = Convert.FromBase64String(sourceString);
            byte[] outputBytes = decryptor.TransformFinalBlock(sourceBytes, 0, sourceBytes.Length);
            return Encoding.UTF8.GetString(outputBytes);
        }

        public static string EncryptFloat(float value)
        {
            byte[] bytes = BitConverter.GetBytes(value);
            string base64 = Convert.ToBase64String(bytes);
            return SimpleEncryption.EncryptString(base64);
        }

        public static string EncryptInt(int value)
        {
            byte[] bytes = BitConverter.GetBytes(value);
            string base64 = Convert.ToBase64String(bytes);
            return SimpleEncryption.EncryptString(base64);
        }

        public static float DecryptFloat(string sourceString)
        {
            string decryptedString = SimpleEncryption.DecryptString(sourceString);
            byte[] bytes = Convert.FromBase64String(decryptedString);
            return BitConverter.ToSingle(bytes, 0);
        }

        public static int DecryptInt(string sourceString)
        {
            string decryptedString = SimpleEncryption.DecryptString(sourceString);
            byte[] bytes = Convert.FromBase64String(decryptedString);
            return BitConverter.ToInt32(bytes, 0);
        }
    }
}