using System;
using System.Globalization;
using UnityEngine;
using Argentics.InkWars.Data.PlayerPrefsExtensions;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Linq;

namespace Argentics.InkWars
{
    public static class PlayerPrefsUtility
    {
        public const string KEY_PREFIX = "ENC-";
        public const string VALUE_FLOAT_PREFIX = "0";
        public const string VALUE_INT_PREFIX = "1";
        public const string VALUE_STRING_PREFIX = "2";

        public static bool IsEncryptedKey(string key)
        {
            if (key.StartsWith(KEY_PREFIX))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static string DecryptKey(string encryptedKey)
        {
            if (encryptedKey.StartsWith(KEY_PREFIX))
            {
                string strippedKey = encryptedKey.Substring(KEY_PREFIX.Length);
                return SimpleEncryption.DecryptString(strippedKey);
            }
            else
            {
                throw new InvalidOperationException("Could not decrypt item, no match found in known encrypted key prefixes");
            }
        }

        #region SetEncryptedData

        public static void SetEncryptedFloat(string key, float value)
        {
            string encryptedKey = SimpleEncryption.EncryptString(key);
            string encryptedValue = SimpleEncryption.EncryptFloat(value);

            PlayerPrefs.SetString(KEY_PREFIX + encryptedKey, VALUE_FLOAT_PREFIX + encryptedValue);
        }

        public static void SetEncryptedInt(string key, int value)
        {
            string encryptedKey = SimpleEncryption.EncryptString(key);
            string encryptedValue = SimpleEncryption.EncryptInt(value);

            PlayerPrefs.SetString(KEY_PREFIX + encryptedKey, VALUE_INT_PREFIX + encryptedValue);
        }

        public static void SetEncryptedString(string key, string value)
        {
            string encryptedKey = SimpleEncryption.EncryptString(key);
            string encryptedValue = SimpleEncryption.EncryptString(value);

            PlayerPrefs.SetString(KEY_PREFIX + encryptedKey, VALUE_STRING_PREFIX + encryptedValue);
        }

        #endregion

        #region GetEncryptedData

        public static object GetEncryptedValue(string encryptedKey, string encryptedValue)
        {
            if (encryptedValue.StartsWith(VALUE_FLOAT_PREFIX))
            {
                return GetEncryptedFloat(SimpleEncryption.DecryptString(encryptedKey.Substring(KEY_PREFIX.Length)));
            }
            else if (encryptedValue.StartsWith(VALUE_INT_PREFIX))
            {
                return GetEncryptedInt(SimpleEncryption.DecryptString(encryptedKey.Substring(KEY_PREFIX.Length)));
            }
            else if (encryptedValue.StartsWith(VALUE_STRING_PREFIX))
            {
                return GetEncryptedString(SimpleEncryption.DecryptString(encryptedKey.Substring(KEY_PREFIX.Length)));
            }
            else
            {
                throw new InvalidOperationException("Could not decrypt item, no match found in known encrypted key prefixes");
            }
        }

        public static float GetEncryptedFloat(string key, float defaultValue = 0.0f)
        {
            string encryptedKey = KEY_PREFIX + SimpleEncryption.EncryptString(key);

            string fetchedString = PlayerPrefs.GetString(encryptedKey);

            if (!string.IsNullOrEmpty(fetchedString))
            {
                fetchedString = fetchedString.Remove(0, 1);

                return SimpleEncryption.DecryptFloat(fetchedString);
            }
            else
            {
                return defaultValue;
            }
        }

        public static int GetEncryptedInt(string key, int defaultValue = 0)
        {
            string encryptedKey = KEY_PREFIX + SimpleEncryption.EncryptString(key);

            string fetchedString = PlayerPrefs.GetString(encryptedKey);

            if (!string.IsNullOrEmpty(fetchedString))
            {
                fetchedString = fetchedString.Remove(0, 1);

                return SimpleEncryption.DecryptInt(fetchedString);
            }
            else
            {
                return defaultValue;
            }
        }

        public static string GetEncryptedString(string key, string defaultValue = "")
        {
            string encryptedKey = KEY_PREFIX + SimpleEncryption.EncryptString(key);

            string fetchedString = PlayerPrefs.GetString(encryptedKey);

            if (!string.IsNullOrEmpty(fetchedString))
            {
                fetchedString = fetchedString.Remove(0, 1);

                return SimpleEncryption.DecryptString(fetchedString);
            }
            else
            {
                return defaultValue;
            }
        }

        #endregion

        #region SimpleNotEncryptedData

        public static void SetBool(string key, bool value)
        {
            if (value)
            {
                PlayerPrefs.SetInt(key, 1);
            }
            else
            {
                PlayerPrefs.SetInt(key, 0);
            }
        }

        public static bool GetBool(string key, bool defaultValue = false)
        {
            if (PlayerPrefs.HasKey(key))
            {
                int value = PlayerPrefs.GetInt(key);

                if (value != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return defaultValue;
            }
        }

        public static void SetEnum(string key, Enum value)
        {
            PlayerPrefs.SetString(key, value.ToString());
        }

        public static T GetEnum<T>(string key, T defaultValue = default(T)) where T : struct
        {
            string stringValue = PlayerPrefs.GetString(key);

            if (!string.IsNullOrEmpty(stringValue))
            {
                return (T)Enum.Parse(typeof(T), stringValue);
            }
            else
            {
                return defaultValue;
            }
        }

        public static object GetEnum(string key, Type enumType, object defaultValue)
        {
            string value = PlayerPrefs.GetString(key);

            if (!string.IsNullOrEmpty(value))
            {
                return Enum.Parse(enumType, value);
            }
            else
            {
                return defaultValue;
            }
        }

        public static void SetDateTime(string key, DateTime value)
        {
            PlayerPrefs.SetString(key, value.ToString("o", CultureInfo.InvariantCulture));
        }

        public static DateTime GetDateTime(string key, DateTime defaultValue = new DateTime())
        {
            string stringValue = PlayerPrefs.GetString(key);

            if (!string.IsNullOrEmpty(stringValue))
            {
                return DateTime.Parse(stringValue, CultureInfo.InvariantCulture, DateTimeStyles.RoundtripKind);
            }
            else
            {
                return defaultValue;
            }
        }

        public static void SetTimeSpan(string key, TimeSpan value)
        {
            PlayerPrefs.SetString(key, value.ToString());
        }

        public static TimeSpan GetTimeSpan(string key, TimeSpan defaultValue = new TimeSpan())
        {
            string stringValue = PlayerPrefs.GetString(key);

            if (!string.IsNullOrEmpty(stringValue))
            {
                return TimeSpan.Parse(stringValue);
            }
            else
            {
                return defaultValue;
            }
        }

        #endregion

        #region Array/Dictionary/List Serialization

        /// <summary>Serializes an object of type T to JSON.</summary>
        /// <returns>A JSON representation of the object T.</returns>
        /// <param name="data">The object T.</param>
        public static void SaveSimpleCollectionToCache<T>(T data, string key) where T : class
        {
            if (typeof(T) == typeof(string[])) { SetEncryptedString(key.ToString(), ToJsonStringArray(data as string[])); }
            else if (typeof(T) == typeof(int[])) { SetEncryptedString(key.ToString(), ToJsonIntArray(data as int[])); }
            else if (typeof(T) == typeof(float[])) { SetEncryptedString(key.ToString(), ToJsonFloatArray(data as float[])); }
            else if (typeof(T) == typeof(Dictionary<string, string>))
            {
                SetEncryptedString(key.ToString(), ToJsonStringStringDictionary(data as Dictionary<string, string>));
            }
            else if (typeof(T) == typeof(Dictionary<string, int>))
            {
                SetEncryptedString(key.ToString(), ToJsonStringIntDictionary(data as Dictionary<string, int>));
            }
            else if (typeof(T) == typeof(Dictionary<string, float>))
            {
                SetEncryptedString(key.ToString(), ToJsonStringFloatDictionary(data as Dictionary<string, float>));
            }
            {
                Debug.LogError(string.Format("Type {0} is not supported.", typeof(T).Name));
            }
        }

        /// <summary>Deserializes an object of type T from JSON.</summary>
        /// <returns>The object T.</returns>
        /// <param name="json">A JSON representation of the object T.</param>
        public static T LoadSimpleCollectionFromCache<T>(string key) where T : class
        {
            if (typeof(T) == typeof(string[])) { return FromJsonStringArray(GetEncryptedString(key.ToString())) as T; }
            if (typeof(T) == typeof(int[])) { return FromJsonIntArray(GetEncryptedString(key.ToString())) as T; }
            if (typeof(T) == typeof(float[])) { return FromJsonFloatArray(GetEncryptedString(key.ToString())) as T; }
            else if (typeof(T) == typeof(Dictionary<string, string>))
            {
                return FromJsonStringStringDictionary(GetEncryptedString(key.ToString())) as T;
            }
            else if (typeof(T) == typeof(Dictionary<string, int>))
            {
                return FromJsonStringIntDictionary(GetEncryptedString(key.ToString())) as T;
            }
            else if (typeof(T) == typeof(Dictionary<string, float>))
            {
                return FromJsonStringFloatDictionary(GetEncryptedString(key.ToString())) as T;
            }else
            {
                Debug.LogError(string.Format("Type {0} is not supported.", typeof(T).Name)); return null;
            }
        }

        #endregion

        #region Arrays

        /* 			string[]			*/

        [System.Serializable]
        private class TopLevelStringArray
        {
            public string[] array;
        }

        private static string ToJsonStringArray(string[] array)
        {
            TopLevelStringArray topLevelArray = new TopLevelStringArray() { array = array };
            return JsonUtility.ToJson(topLevelArray);
        }

        private static string[] FromJsonStringArray(string json)
        {
            TopLevelStringArray topLevelArray = JsonUtility.FromJson<TopLevelStringArray>(json);
            return topLevelArray.array;
        }

        /*			int[]				*/

        [System.Serializable]
        private class TopLevelIntArray
        {
            public int[] array;
        }

        private static string ToJsonIntArray(int[] array)
        {
            TopLevelIntArray topLevelArray = new TopLevelIntArray() { array = array };
            return JsonUtility.ToJson(topLevelArray);
        }

        private static int[] FromJsonIntArray(string json)
        {
            TopLevelIntArray topLevel = JsonUtility.FromJson<TopLevelIntArray>(json);
            return topLevel.array;
        }

        /*			float[]				*/

        [System.Serializable]
        private class TopLevelFloatArray
        {
            public float[] array;
        }

        private static string ToJsonFloatArray(float[] array)
        {
            TopLevelFloatArray topLevelArray = new TopLevelFloatArray() { array = array };
            return JsonUtility.ToJson(topLevelArray);
        }

        private static float[] FromJsonFloatArray(string json)
        {
            TopLevelFloatArray topLevel = JsonUtility.FromJson<TopLevelFloatArray>(json);
            return topLevel.array;
        }

        #endregion

        #region Dictionaries

        /* 			Dictionary<string, string>			*/

        [System.Serializable]
        private class StringStringDictionaryArray
        {
            public StringStringDictionary[] items;
        }

        [System.Serializable]
        private class StringStringDictionary
        {
            public string key;
            public string value;
        }

        private static string ToJsonStringStringDictionary(Dictionary<string, string> dictionary)
        {
            List<StringStringDictionary> dictionaryItemsList = new List<StringStringDictionary>();
            foreach (KeyValuePair<string, string> kvp in dictionary)
            {
                dictionaryItemsList.Add(new StringStringDictionary() { key = kvp.Key, value = kvp.Value });
            }

            StringStringDictionaryArray dictionaryArray = new StringStringDictionaryArray() { items = dictionaryItemsList.ToArray() };
            return JsonUtility.ToJson(dictionaryArray);
        }

        private static Dictionary<string, string> FromJsonStringStringDictionary(string json)
        {
            StringStringDictionaryArray loadedData = JsonUtility.FromJson<StringStringDictionaryArray>(json);
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            for (int i = 0; i < loadedData.items.Length; i++)
            {
                dictionary.Add(loadedData.items[i].key, loadedData.items[i].value);
            }
            return dictionary;
        }

        /* 			Dictionary<string, float>			*/

        [System.Serializable]
        private class StringIntDictionaryArray
        {
            public StringIntDictionary[] items;
        }

        [System.Serializable]
        private class StringIntDictionary
        {
            public string key;
            public int value;
        }

        private static string ToJsonStringIntDictionary(Dictionary<string, int> dictionary)
        {
            List<StringIntDictionary> dictionaryItemsList = new List<StringIntDictionary>();
            foreach (KeyValuePair<string, int> kvp in dictionary)
            {
                dictionaryItemsList.Add(new StringIntDictionary() { key = kvp.Key, value = kvp.Value });
            }

            StringIntDictionaryArray dictionaryArray = new StringIntDictionaryArray() { items = dictionaryItemsList.ToArray() };
            return JsonUtility.ToJson(dictionaryArray);
        }

        private static Dictionary<string, int> FromJsonStringIntDictionary(string json)
        {
            StringIntDictionaryArray loadedData = JsonUtility.FromJson<StringIntDictionaryArray>(json);
            Dictionary<string, int> dictionary = new Dictionary<string, int>();
            for (int i = 0; i < loadedData.items.Length; i++)
            {
                dictionary.Add(loadedData.items[i].key, loadedData.items[i].value);
            }
            return dictionary;
        }

        /* 			Dictionary<string, float>			*/

        [System.Serializable]
        private class StringFloatDictionaryArray
        {
            public StringFloatDictionary[] items;
        }

        [System.Serializable]
        private class StringFloatDictionary
        {
            public string key;
            public float value;
        }

        private static string ToJsonStringFloatDictionary(Dictionary<string, float> dictionary)
        {
            List<StringFloatDictionary> dictionaryItemsList = new List<StringFloatDictionary>();
            foreach (KeyValuePair<string, float> kvp in dictionary)
            {
                dictionaryItemsList.Add(new StringFloatDictionary() { key = kvp.Key, value = kvp.Value });
            }

            StringFloatDictionaryArray dictionaryArray = new StringFloatDictionaryArray() { items = dictionaryItemsList.ToArray() };
            return JsonUtility.ToJson(dictionaryArray);
        }

        private static Dictionary<string, float> FromJsonStringFloatDictionary(string json)
        {
            StringFloatDictionaryArray loadedData = JsonUtility.FromJson<StringFloatDictionaryArray>(json);
            Dictionary<string, float> dictionary = new Dictionary<string, float>();
            for (int i = 0; i < loadedData.items.Length; i++)
            {
                dictionary.Add(loadedData.items[i].key, loadedData.items[i].value);
            }
            return dictionary;
        }

        #endregion

        #region List
        public static void SaveListString(List<string> data, string key)
        {
            SetEncryptedString(key.ToString(), String.Join(",", data.ToArray()));
        }

        public static List<string> LoadListString(string key)
        {
            return GetEncryptedString(key.ToString()).Split(',').ToList();
        }
#endregion

        #region Class Instance & File


        public static T LoadFromFile<T>(string key) where T : class
        {

            string path = PathForFilename(key.ToString());
            if (PathExists(path))
            {
                return JsonUtility.FromJson<T>(File.ReadAllText(path));
            }
            else return default(T);

        }


        public static void SaveToFile<T>(string key, T data) where T : class
        {
            string path = PathForFilename(key.ToString());
            File.WriteAllText(path, JsonUtility.ToJson(data));
        }


        private static bool PathExists(string filepath)
        {
            return File.Exists(filepath);
        }


        public static bool FileExists(string filename)
        {
            return PathExists(PathForFilename(filename));
        }


        public static void DeleteFile(string filename)
        {
            string filepath = PathForFilename(filename);
            if (PathExists(filepath))
            {
                File.Delete(filepath);
            }
        }

        private static string PathForFilename(string filename)
        {
            string path = filename; //for editor
#if UNITY_STANDALONE
            path = Path.Combine(Application.dataPath, filename);
#elif UNITY_IOS || UNITY_ANDROID
            path = Path.Combine(Application.persistentDataPath, filename);
#endif
            return path;
        }
        #endregion

        #region Class Instance & Cache

        public static T LoadFromCache<T>(string DataID) where T : class
        {

            return (T)DeserializeObject(PlayerPrefs.GetString(DataID));
        }

        public static void SaveToCache<T>(string DataID, T data) where T : class
        {
            PlayerPrefs.SetString(DataID, SerializeObject(data));
        }

        #endregion

        #region Check

        public static bool IsCacheDataExist(string DataID)
        {
            return PlayerPrefsUtility.HasKey(DataID);
        }

        public static bool IsFileDataExist(string key)
        {
            return FileExists(key.ToString());
        }

        private static bool HasEncryptedKey(string key)
        {
            string encryptedKey = KEY_PREFIX + SimpleEncryption.EncryptString(key);

            return PlayerPrefs.HasKey(encryptedKey);
        }

        public static bool HasKey(string key)
        {
            if (PlayerPrefs.HasKey(key) || HasEncryptedKey(key))
            {
                return true;
            }
            else
            {
                return false;
            }

        }



        #endregion


        #region Binary Serrialization
        public static string SerializeObject(object o)
        {
            if (!o.GetType().IsSerializable)
            {
                return null;
            }

            using (MemoryStream stream = new MemoryStream())
            {
                new BinaryFormatter().Serialize(stream, o);
                return Convert.ToBase64String(stream.ToArray());
            }
        }

        public static object DeserializeObject(string str)
        {
            byte[] bytes = Convert.FromBase64String(str);

            using (MemoryStream stream = new MemoryStream(bytes))
            {
                return new BinaryFormatter().Deserialize(stream);
            }
        }
        #endregion
    }
}