﻿using Argentics.InkWars.Game.Player;

namespace Argentics.InkWars.Data
{
    public class DataProvider
    {
        public PlayerData ThisPlayerData { get; set; }
    }
}
