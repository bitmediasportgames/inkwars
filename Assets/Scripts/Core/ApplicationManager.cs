﻿using Argentics.InkWars.Core;
using Argentics.InkWars.Data;
using Argentics.InkWars.Game.BattleCore;
using Argentics.InkWars.Game.BattleCore.BattleComponents.Shooting.PaintingProjectiles;
using Argentics.InkWars.Game.Player;
using Argentics.InkWars.Utils;
using UnityEngine;

namespace Argentics.InkWars
{
    [RequireComponent(typeof(SceneLoader))]
    public class ApplicationManager : MonoBehaviour
    {
        [SerializeField] private BattleManager _battleManagerPrefab;

        private SceneLoader _sceneLoader;
        private MenuManager _menuManager;

        private DataProvider _dataProvider;

        private BattleManager _battleManager;

        private void Awake()
        {
            DontDestroyOnLoad(gameObject);

            InitializeComponents();
        }

        private void Start()
        {
            _sceneLoader.LoadScene(GameScene.Menu, new SceneConfig());
        }

        private void InitializeComponents()
        {
            _sceneLoader = GetComponent<SceneLoader>();
            _sceneLoader.Initialize();
            _sceneLoader.SceneLoaded += OnSceneLoaded;
        }

        private void OnSceneLoaded(GameScene sceneType, SceneConfig sceneConfig)
        {
            switch (sceneType)
            {
                case GameScene.Menu:
                    InitializeMenuScene(sceneConfig);
                    break;
                case GameScene.Game:
                    InitializeBattleScene(sceneConfig);
                    break;
                default:
                    Debug.LogWarning("No OnSceneLoaded callback for " + sceneType + ".");
                    break;
            }
        }

        private void InitializeMenuScene(SceneConfig menuSceneConfig)
        {
            if (_menuManager == null)
            {
                _menuManager = new MenuManager();
                _menuManager.BattleStartRequest += OnBattleStartRequested;
            }

            StartCoroutine(_menuManager.InitializeCoroutine(menuSceneConfig as MenuSceneConfig));
        }

        private void InitializeBattleScene(SceneConfig gameSceneConfig)
        {
            var targetConfig = (BattleSceneConfig) gameSceneConfig;

            if (_battleManager == null)
            {
                _battleManager = GameObjectCreator.CreateGameObjectWithComponent(_battleManagerPrefab);

                _battleManager.BattleEnded += OnBattleEnded;
            }

            var thisPlayerData = new PlayerData
            {
                AvailableProjectileTypes = new[]
                    {PaintingProjectileType.Simple, PaintingProjectileType.Accurate, PaintingProjectileType.MegaImpact},
                Color = _battleManager.AvailableColors[0],
                Name = "Bilbo",
                ThisPlayer = true
            };

            var oppositePlayerData = new PlayerData
            {
                AvailableProjectileTypes = new[]
                    {PaintingProjectileType.Simple, PaintingProjectileType.Accurate, PaintingProjectileType.MegaImpact},
                Color = _battleManager.AvailableColors[1],
                Name = "Lalana",
                ThisPlayer = false
            };

            var battleConfig = new BattleConfig
            {
                BattleMode = BattleMode.SinglePlayer,
                BattleType = BattleType.LevelPainting,
                ThisPlayerData = thisPlayerData,
                OppositePlayerData = oppositePlayerData
            };

            battleConfig.LevelId = targetConfig.BattleConfig.LevelId;

            _battleManager.Initialize(battleConfig);
        }

        #region Callbacks

        private void OnBattleStartRequested(BattleConfig battleConfig)
        {
            _sceneLoader.LoadSceneAsync(GameScene.Game, new BattleSceneConfig { BattleConfig = battleConfig });
        }

        private void OnBattleEnded(BattleResultData resultData)
        {
            //из которого потом достанет инфу ModeManager
            _sceneLoader.LoadSceneAsync(GameScene.Menu, new MenuSceneConfig { BattleResultData = resultData });
        }

        #endregion
    }
}
