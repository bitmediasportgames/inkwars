﻿using System;
using System.Collections;
using Argentics.InkWars.Game.BattleCore;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Argentics.InkWars.Core
{
    public enum GameScene
    {
        Splash,
        Menu,
        Game
    }

    public class SceneLoader : MonoBehaviour
    {
        private const string SPLASH_SCENE_NAME = "SplashScene";
        private const string MENU_SCENE_NAME = "MenuScene";
        private const string GAME_SCENE_NAME = "BattleScene";

        public event Action<GameScene, SceneConfig> SceneLoaded;

        private SceneConfig _currentLoadingSceneConfig;
        private bool _sceneLoading;

        public void Initialize()
        {
            _sceneLoading = false;
            SceneManager.sceneLoaded += OnSceneLoaded;
        }

        private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
        {
            if (SceneLoaded != null)
            {
                SceneLoaded(SceneNameToEnum(scene.name), _currentLoadingSceneConfig);
            }

            _sceneLoading = false;
            _currentLoadingSceneConfig = null;
        }

        public void LoadScene(GameScene sceneToLoad, SceneConfig config)
        {
            if (_sceneLoading)
            {
                throw new Exception("Already loading a scene.");
            }

            _sceneLoading = true;
            _currentLoadingSceneConfig = config;

            SceneManager.LoadScene(EnumToSceneName(sceneToLoad));
        }

        public void LoadSceneAsync(GameScene sceneToLoad, SceneConfig config = null)
        {
            if (_sceneLoading)
            {
                throw new Exception("Already loading a scene.");
            }

            _sceneLoading = true;
            _currentLoadingSceneConfig = config;

            StartCoroutine(AsyncSceneLoad(EnumToSceneName(sceneToLoad)));
        }

        private IEnumerator AsyncSceneLoad(string sceneName)
        {
            AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(sceneName);

            while (!asyncLoad.isDone)
            {
                yield return null;
            }
        }

        private string EnumToSceneName(GameScene scene)
        {
            switch (scene)
            {
                case GameScene.Splash:
                    return SPLASH_SCENE_NAME;
                case GameScene.Menu:
                    return MENU_SCENE_NAME;
                case GameScene.Game:
                    return GAME_SCENE_NAME;
                default:
                    throw new System.NotImplementedException("Scene name for " + scene + " not set.");
            }
        }

        private GameScene SceneNameToEnum(string name)
        {
            switch(name)
            {
                case SPLASH_SCENE_NAME:
                    return GameScene.Splash;
                case MENU_SCENE_NAME:
                    return GameScene.Menu;
                case GAME_SCENE_NAME:
                    return GameScene.Game;
                default:
                    throw new NotImplementedException(@"Scene type for name """ + name + @""" not set.");
            }
        }
    }
}

namespace Argentics.InkWars
{
    public class SceneConfig
    {
    }

    public class MenuSceneConfig : SceneConfig
    {
        public BattleResultData BattleResultData { get; set; }
    }

    public class BattleSceneConfig : SceneConfig
    {
        public BattleConfig BattleConfig { get; set; }
    }
}