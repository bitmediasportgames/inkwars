﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Argentics.InkWars.Menu.UI.Popups
{
    public class MessageBoxPopup : BasePopup
    {
        public override PopupType Type
        {
            get
            {
                return PopupType.MessageBox;
            }
        }
    }
}