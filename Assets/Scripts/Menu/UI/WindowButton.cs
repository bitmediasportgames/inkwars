﻿namespace Argentics.InkWars.Menu.UI
{
    public class WindowButton : EnumButton
    {
        public WindowType WindowType;

        public override void ShowWindow()
        {
            UIManager.ShowWindow(WindowType);
        }
    }
}