﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Argentics.InkWars.Menu.UI
{
    public enum WindowType
    {
        Nested,
        Main,
        Options,
        Lobby,
        Mode,
        Club,
        FootballPlayerInfo,
        ClubBuildings
    }

    public abstract class BaseWindow : MonoBehaviour, IShowable
    {
        public event System.Action<WindowType> Shown;
        public event System.Action<WindowType> Hidden;

        public abstract WindowType Type { get; }
        public int ID { get { return (int)Type; } }

        protected UIManager _uiManager;

        public virtual IEnumerator Initialize(UIManager uIManager)
        {
            _uiManager = uIManager;
            yield return null;
        }

        public virtual IEnumerator Show(ShowOptions showOptions)
        {
            gameObject.SetActive(true);
            yield return null;
            OnShown();
        }

        /// <summary>
        /// Triggers Shown(WindowType) event.
        /// </summary>
        protected virtual void OnShown()
        {
            if (Shown != null)
                Shown(Type);
        }

        public virtual IEnumerator Hide()
        {
            gameObject.SetActive(false);
            yield return null;
            OnHidden();
        }

        /// <summary>
        /// Triggers Hidden(WindowType) event.
        /// </summary>
        protected virtual void OnHidden()
        {
            if (Hidden != null)
                Hidden(Type);
        }
    }
}