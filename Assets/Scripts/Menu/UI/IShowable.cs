﻿using System.Collections;

namespace Argentics.InkWars.Menu.UI
{
    public interface IShowable
    {
        IEnumerator Initialize(UIManager uIManager);
        IEnumerator Show(ShowOptions showOptions);
        IEnumerator Hide();
    }

    public class ShowOptions
    {
        public ShowParams ShowParams;
    }

    public enum ShowParams
    {
        Animated,
        NoAnimation
    }
}