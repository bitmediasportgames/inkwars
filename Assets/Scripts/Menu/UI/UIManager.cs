﻿using System.Collections;
using System.Collections.Generic;
using Argentics.InkWars.Game.BattleCore;
using UnityEngine;

namespace Argentics.InkWars.Menu.UI
{
    public class UIManager : MonoBehaviour
    {
        public event System.Action<BattleConfig> BattleStartRequest;

        [SerializeField] private Transform _windowsContainer;
        [SerializeField] private Transform _popupsContainer;
        [SerializeField] private GameObject _popupDarkBackground;

        private Dictionary<WindowType, BaseWindow> _windows;
        private Dictionary<PopupType, BasePopup> _popups;

        private Stack<BaseWindow> _openedWindows;
        private Stack<BasePopup> _openedPopups;

#if UNITY_EDITOR

        [Space, Header("Editor only"), SerializeField]
        private ApplicationManager _reserveAppManagerPrefab;

        private void Start()
        {
            if(FindObjectOfType<ApplicationManager>() == null)
            {
                Debug.LogWarning("Started menu scene without splash - creating Application Manager directly.");
                Instantiate(_reserveAppManagerPrefab);
            }
        }
#endif

        #region  inits
        public IEnumerator Initialize(MenuSceneConfig config)
        {
            _openedWindows = new Stack<BaseWindow>();
            _openedPopups = new Stack<BasePopup>();

            yield return InitWindows();
            yield return InitPopups();

            if (config != null)
            {
                if (config.BattleResultData != null)
                {
                    //save match result to DataProvider
                    Debug.Log("returned from match. results: ");
                }
                else
                {
                    ShowWindow(WindowType.Main);
                }
            }
            else
            {
                Debug.Log("menu from splash");
                ShowWindow(WindowType.Main);
            }
        }

        private IEnumerator InitWindows()
        {
            var windows = _windowsContainer.GetComponentsInChildren<BaseWindow>(true);

            _windows = new Dictionary<WindowType, BaseWindow>();
            for (int i = 0; i < windows.Length; i++)
            {
                if (_windows.ContainsKey(windows[i].Type))
                {
                    Debug.LogError("Duplicated window: " + windows[i].Type);
                    continue;
                }
                if(windows[i].Type == WindowType.Nested) //do not add nested windows
                {
                    continue;
                }
                _windows.Add(windows[i].Type, windows[i]);
                yield return windows[i].Initialize(this);
            }
            yield return null;
        }

        private IEnumerator InitPopups()
        {
            var popups = _popupsContainer.GetComponentsInChildren<BasePopup>(true);
            _popups = new Dictionary<PopupType, BasePopup>();
            for (int i = 0; i < popups.Length; i++)
            {
                if (_popups.ContainsKey(popups[i].Type))
                {
                    Debug.LogError("Duplicated popup: " + popups[i].Type);
                    continue;
                }
                _popups.Add(popups[i].Type, popups[i]);
                yield return popups[i].Initialize(this);
            }
            yield return null;
        }
        #endregion

        #region windows logic
        public void ShowWindowByID(int windowID)
        {
            StartCoroutine(ShowWindowRoutine(GetWindowByID(windowID)));
        }

        public void ShowWindowByID(int windowID, ShowOptions showOptions = null)
        {
            StartCoroutine(ShowWindowRoutine(GetWindowByID(windowID), showOptions));
        }

        private BaseWindow GetWindowByID(int id)
        {
            IEnumerator enumerator = _windows.Values.GetEnumerator();

            while (enumerator.MoveNext())
            {
                BaseWindow window = enumerator.Current as BaseWindow;

                if (window.ID == id)
                {
                    return window;
                }
            }

            throw new System.Exception("Window with ID " + id + " not found.");
        }

        public void ShowWindow(WindowType windowToShow, ShowOptions showOptions = null)
        {
            if (windowToShow == WindowType.Nested)
            {
                throw new System.ArgumentException("You are not allowed to show " + windowToShow + " explicitly. Use ShowModeWindow insted.");
            }

            if (!_windows.ContainsKey(windowToShow))
            {
                throw new System.Exception("Window " + windowToShow + " not found.");
            }

            StartCoroutine(ShowWindowRoutine(_windows[windowToShow], showOptions));
        }

        private IEnumerator ShowWindowRoutine(BaseWindow windowToShow, ShowOptions showOptions = null)
        {
            if (_openedWindows.Contains(windowToShow))
            {
                throw new System.Exception("Could not open " + windowToShow.Type + ": it is already opened(in stack).");
            }

            if (_openedWindows.Count > 0)
            {
                yield return _openedWindows.Peek().Hide();
            }

            yield return windowToShow.Show(showOptions);

            _openedWindows.Push(windowToShow);
        }

        private IEnumerator HideWindowRoutine()
        {
            if (_openedWindows.Count == 0)
            {
                throw new System.Exception("Hide window failed: nothing to hide.");
            }

            yield return _openedWindows.Pop().Hide();

            if (_openedWindows.Count > 0)
            {
                yield return _openedWindows.Peek().Show(new ShowOptions());
            }

            else yield return ShowWindowRoutine(_windows[WindowType.Main]);
        }
        #endregion

        #region popups logic

        public void ShowPopupByID(int popupID)
        {
            ShowPopupByID(popupID, null);
        }

        public void ShowPopupByID(int popupID, ShowOptions showOptions)
        {
            StartCoroutine(ShowPopupRoutine(GetPopupByID(popupID), showOptions));
        }

        private BasePopup GetPopupByID(int id)
        {
            IEnumerator enumerator = _popups.Values.GetEnumerator();

            while (enumerator.MoveNext())
            {
                BasePopup popup = enumerator.Current as BasePopup;

                if (popup.ID == id)
                {
                    return popup;
                }
            }

            throw new System.Exception("Window with ID " + id + " not found.");
        }

        public void ShowPopup(PopupType popupToShow, ShowOptions showOptions = null)
        {
            StartCoroutine(ShowPopupRoutine(_popups[popupToShow], showOptions));
        }

        private IEnumerator ShowPopupRoutine(BasePopup popupToShow, ShowOptions showOptions)
        {
            if (_openedPopups.Contains(popupToShow))
            {
                throw new System.Exception("Could not open " + popupToShow.Type + ": it is already opened(in stack).");
            }

            if (showOptions == null)
            {
                showOptions = new ShowOptions();
            }

            yield return popupToShow.Show(showOptions);

            _openedPopups.Push(popupToShow);
            _popupDarkBackground.SetActive(true);
        }

        private IEnumerator HidePopupRoutine()
        {
            if (_openedWindows.Count == 0)
            {
                throw new System.Exception("HidePopup failed: not enough opened popups.");
            }

            yield return _openedPopups.Pop().Hide();

            _popupDarkBackground.SetActive(_openedPopups.Count > 0);
        }
        #endregion

        #region callbacks

        public void OnMatchStartRequested(BattleConfig battleConfig)
        {
            if (BattleStartRequest != null)
            {
                BattleStartRequest(battleConfig);
            }
        }

        #endregion

        public void OnBackPressed()
        {
            if (_openedPopups.Count > 0)
            {
                StartCoroutine(HidePopupRoutine());
                return;
            }

            if (_openedWindows.Peek().Type != WindowType.Main)
            {
                StartCoroutine(HideWindowRoutine());
            }
            else
            {
#if UNITY_EDITOR
                UnityEditor.EditorApplication.isPlaying = false;
#endif
                Application.Quit();
            }
        }

        private void Update()
        {
            if(Input.GetKeyDown(KeyCode.Escape))
            {
                OnBackPressed();
            }
        }
    }
}