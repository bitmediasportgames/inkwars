﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Argentics.InkWars.Menu.UI
{
    //обходим ограничение юнити использовать енамы всеми возможными способами
    public abstract class EnumButton : MonoBehaviour
    {
        public UIManager UIManager { get; set; }

        public abstract void ShowWindow();
    }
}