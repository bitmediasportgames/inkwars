﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Argentics.InkWars.Menu.UI.Extensions
{
    public class OneDirectionScroll : ScrollRect
    {
        private bool _shouldDrag;

        public override void OnBeginDrag(PointerEventData eventData)
        {
            base.OnBeginDrag(eventData);
            if (horizontal && Mathf.Abs(eventData.delta.x) > Mathf.Abs(eventData.delta.y))
            {
                _shouldDrag = true;
            }
            else if (vertical && Mathf.Abs(eventData.delta.y) > Mathf.Abs(eventData.delta.x))
                _shouldDrag = true;
            else
                _shouldDrag = false;
        }

        public override void OnDrag(PointerEventData eventData)
        {
            if (!_shouldDrag)
                return;
            base.OnDrag(eventData);
        }
    }
}