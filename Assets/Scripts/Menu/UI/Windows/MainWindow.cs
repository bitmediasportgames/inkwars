﻿using System.Collections;
using Argentics.InkWars.Game.BattleCore;
using UnityEngine;

namespace Argentics.InkWars.Menu.UI.Windows
{
    public class MainWindow : BaseWindow
    {
        public override WindowType Type
        {
            get
            {
                return WindowType.Main;
            }
        }
        [SerializeField]
        private Transform _modeButtonsContainer;

        private UIManager _thisUIManager;
        
        //private ModeButton[] _modeButtons;

        public override IEnumerator Initialize(UIManager uIManager)
        {
            _thisUIManager = uIManager;

            yield return base.Initialize(uIManager);
            //_modeButtons = _modeButtonsContainer.GetComponentsInChildren<ModeButton>(true);

            //for (int i = 0; i < _modeButtons.Length; i++)
            //{
            //    _modeButtons[i].UIManager = uIManager;
            //}
        }

        public void OnLevelButtonClicked(int levelIndex)
        {
            _thisUIManager.OnMatchStartRequested(new BattleConfig
            {
                LevelId = levelIndex
            });
        }
    }
}