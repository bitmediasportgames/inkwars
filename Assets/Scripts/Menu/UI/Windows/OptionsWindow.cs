﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Argentics.InkWars.Menu.UI.Windows
{
    public class OptionsWindow : BaseWindow
    {
        public override WindowType Type
        {
            get
            {
                return WindowType.Options;
            }
        }
    }
}