﻿namespace Argentics.InkWars.Menu.UI.Windows
{
    public class LobbyWindow : BaseWindow
    {
        public override WindowType Type
        {
            get
            {
                return WindowType.Lobby;
            }
        }
    }
}