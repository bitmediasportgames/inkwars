﻿using System.Collections;
using Argentics.InkWars.Game.BattleCore;
using UnityEngine;
using Argentics.InkWars.Menu.UI;

namespace Argentics.InkWars
{
    public class MenuManager
    {
        public event System.Action<BattleConfig> BattleStartRequest;

        private UIManager _uiManager;

        public MenuManager()
        {
        }

        public IEnumerator InitializeCoroutine(MenuSceneConfig config)
        {
            _uiManager = GameObject.FindObjectOfType<UIManager>();

            if (_uiManager == null)
            {
                throw new System.Exception("UI manager not found.");
            }
            _uiManager.BattleStartRequest += OnBattleStartRequested;

            if (_uiManager != null)
            {
                yield return _uiManager.Initialize(config);
            }
            else
            {
                throw new System.Exception("Will not init: UIManager is null.");
            }

            yield return null;
        }

        private void Dispose()
        {
            _uiManager.BattleStartRequest -= OnBattleStartRequested;
            _uiManager = null;
        }

        private void OnBattleStartRequested(BattleConfig battleConfig)
        {
            Dispose();

            if (BattleStartRequest != null)
            {
                BattleStartRequest(battleConfig);
            }
        }
    }

    public class GameParams
    {
    }
}