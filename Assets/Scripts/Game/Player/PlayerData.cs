﻿using System;
using Argentics.InkWars.Game.BattleCore.BattleComponents.Shooting.PaintingProjectiles;
using UnityEngine;

namespace Argentics.InkWars.Game.Player
{
    [Serializable]
    public class PlayerData
    {
        public string Name { get; set; }
        public Color Color { get; set; }
        public PaintingProjectileType[] AvailableProjectileTypes { get; set; }
        public bool ThisPlayer { get; set; }
        //public CannonType[] AvailableCannonTypes { get; set; }
    }
}
