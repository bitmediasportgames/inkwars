﻿using UnityEngine;

namespace Argentics.InkWars.Game.BattleCore.BattleCamera
{
    public class BattleCameraController : MonoBehaviour
    {
        [SerializeField] private float _zoomOutMin = 15;
        [SerializeField] private float _zoomOutMax = 60;

        private Vector3 _touchStart;

        private Camera _thisCamera;

        private bool _initialized;

        public void Initialize()
        {
            _thisCamera = GetComponentInChildren<Camera>();

            if (_thisCamera)
            {
                _initialized = true;
            }

            if (!gameObject.activeSelf)
            {
                gameObject.SetActive(true);
            }
        }

        //TODO: temp solution; transfer to input handler then
        private void Update()
        {
            if (!_initialized)
            {
                return;
            }

            if (Input.GetMouseButtonDown(0))
            {
                var screenRay = Camera.main.ScreenPointToRay(Input.mousePosition);

                RaycastHit hit;

                if (Physics.Raycast(screenRay, out hit))
                {
                    if (hit.collider.CompareTag("InkGenerator") 
                    || hit.collider.CompareTag("Cannon"))
                    {
                        return;
                    }
                }

                _touchStart = _thisCamera.ScreenToWorldPoint(Input.mousePosition);
            }
            if (Input.touchCount == 2)
            {
                var touchZero = Input.GetTouch(0);
                var touchOne = Input.GetTouch(1);

                var touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
                var touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

                var prevMagnitude = (touchZeroPrevPos - touchOnePrevPos).magnitude;
                var currentMagnitude = (touchZero.position - touchOne.position).magnitude;

                var difference = currentMagnitude - prevMagnitude;

                Zoom(difference * 0.01f);
            }
            else if (Input.GetMouseButton(0))
            {
                var screenRay = Camera.main.ScreenPointToRay(Input.mousePosition);

                RaycastHit hit;

                if (Physics.Raycast(screenRay, out hit))
                {
                    if (hit.collider.CompareTag("InkGenerator")
                        || hit.collider.CompareTag("Cannon"))
                    {
                        return;
                    }
                }

                var direction = _touchStart - _thisCamera.ScreenToWorldPoint(Input.mousePosition);

                _thisCamera.transform.position += direction;
            }

            #if UNITY_EDITOR

            Zoom(Input.GetAxis("Mouse ScrollWheel"));

            #endif
        }

        private void Zoom(float increment)
        {
            if (_thisCamera.orthographic)
            {
                _thisCamera.orthographicSize = Mathf.Clamp(_thisCamera.orthographicSize - increment, _zoomOutMin, _zoomOutMax);
            }
            else
            {
                _thisCamera.fieldOfView = Mathf.Clamp(_thisCamera.fieldOfView - increment, _zoomOutMin, _zoomOutMax);
            }
        }
    }
}
