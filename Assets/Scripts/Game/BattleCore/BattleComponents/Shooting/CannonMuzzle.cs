﻿using Argentics.InkWars.Game.BattleCore.BattleComponents.Shooting.PaintingProjectiles;
using UnityEngine;

namespace Argentics.InkWars.Game.BattleCore.BattleComponents.Shooting
{
    public class CannonMuzzle : MonoBehaviour
    {
        public Transform ProjectileOutgoingTransform
        {
            get { return _projectileOutgoingTransform; }
        }

        public PaintingProjectileType PaintingProjectileType
        {
            get { return _paintingProjectileType; }
        }

        [SerializeField] private Transform _projectileOutgoingTransform;
        [SerializeField] private PaintingProjectileType _paintingProjectileType;

        public void SetActiveState(bool state)
        {
            gameObject.SetActive(state);
        }
    }
}
