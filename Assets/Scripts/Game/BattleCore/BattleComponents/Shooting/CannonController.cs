﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Argentics.InkWars.Game.BattleCore.BattleComponents.InkGenerators;
using Argentics.InkWars.Game.BattleCore.BattleComponents.Shooting.PaintingProjectiles;
using Argentics.InkWars.Game.BattleCore.BattleComponents.Shooting.Trajectory;
using Argentics.InkWars.Game.BattleCore.Environment;
using Argentics.InkWars.Game.Player;
using Argentics.InkWars.Global;
using CustomDebugDll;
using UnityEngine;

namespace Argentics.InkWars.Game.BattleCore.BattleComponents.Shooting
{
    public enum CannonLocation
    {
        Top,
        Down
    }

    [RequireComponent(typeof(Animator), typeof(TrajectoryDrawer))]
    public class CannonController : MonoBehaviour
    {
        //TODO: InkProvider
        //1) can assign ink generators
        //2) reload time

        public BattleLevelPainter LevelPainter
        {
            get { return _levelPainter; }
        }

        public PaintingProjectilePool[] ProjectilePools { get; private set; }

        public PaintingProjectile[] AvailableProjectiles
        {
            get
            {
                var projectilesList = new List<PaintingProjectile>();

                for (int i = 0; i < ProjectilePools.Length; i++)
                {
                    projectilesList.AddRange(ProjectilePools[i].PaintingProjectiles);
                }

                return projectilesList.ToArray();
            }
        }

        public PaintingProjectileType NextProjectileType { get; private set; }

        //TODO: temporary
        [SerializeField] private PaintingProjectile[] _paintingProjectilePrefabs;

        [SerializeField] private float _reloadTime = 2f;

        [SerializeField] private CannonMuzzle[] _availableMuzzles;

        [SerializeField] private Transform _rotatableTransform;

        private Animator _thisAnimator;

        private TrajectoryDrawer _trajectoryDrawer;

        private CannonMuzzle _activeMuzzle;
        private PaintingProjectile _nextPaintingProjectile;
        private BattleLevelPainter _levelPainter;

        private Color _projectilesColor;

        private Vector3 _shotDirection;

        private CannonLocation _cannonLocation;

        private bool _canShoot;

        private const int DefaultPoolProjectilesCount = 10;
        private const string ShotAnimationTrigger = "shotTrigger";

        private const string CannonTag = Globals.Constants.Tags.CannonTag;

        public void Initialize(PlayerData playerData, BattleLevelPainter levelPainter)
        {
            _thisAnimator = GetComponent<Animator>();
            _trajectoryDrawer = GetComponent<TrajectoryDrawer>();
            _trajectoryDrawer.Initialize();

            //TODO: get color from SO by color id
            _projectilesColor = playerData.Color;

            _cannonLocation = playerData.ThisPlayer ? CannonLocation.Down : CannonLocation.Top;

            InitializeProjectilePools(playerData.AvailableProjectileTypes);

            _levelPainter = levelPainter;
            _levelPainter.AddProjectileSubjects(AvailableProjectiles);

            PrepareNextProjectile();

            if (!gameObject.activeSelf)
            {
                gameObject.SetActive(true);
            }

            _canShoot = true;

            InkGenerator.Captured += SignInkGenerator;

            CustomDebugLog.Message(this, "Initialized!");
        }

        public void Shoot()
        {
            if (!_levelPainter.EnoughInk(_nextPaintingProjectile.InkCost))
            {
                CustomDebugLog.Message("[CannonController]: Not enough ink!");

                return;
            }

            if (!_canShoot)
            {
                CustomDebugLog.Message("[CannonController] Reloading...");

                return;
            }

            StartCoroutine(ShootCoroutine());
        }

        public void OnSimpleRequested()
        {
            SetNextProjectileType(PaintingProjectileType.Simple);
        }

        public void OnAccurateRequested()
        {
            SetNextProjectileType(PaintingProjectileType.Accurate);
        }

        public void OnMegaRequested()
        {
            SetNextProjectileType(PaintingProjectileType.MegaImpact);
        }

        private void SignInkGenerator(InkGeneratorCapturedData inkGeneratorCapturedData)
        {
            _levelPainter.IncreaseInkRestoringSpeed(inkGeneratorCapturedData.InkMultiplyValue);
        }

        private IEnumerator ShootCoroutine()
        {
            _canShoot = false;

            ShootProjectile();
            PlayShotAnimation();

            PrepareNextProjectile();

            yield return new WaitForSecondsRealtime(_reloadTime);

            _canShoot = true;
        }

        private void SetNextProjectileType(PaintingProjectileType type)
        {
            NextProjectileType = type;

            PrepareNextProjectile();

            CustomDebugLog.MessageFormat("[CannonController]: Changed projectile type to: {0}", type);
        }

        private void ShootProjectile()
        {
            _nextPaintingProjectile.gameObject.SetActive(true);

            _nextPaintingProjectile.PrepareForShot(_activeMuzzle.ProjectileOutgoingTransform);

            var force = _nextPaintingProjectile.Speed;
            
            //TODO: temp for top cannon
            if (_shotDirection == Vector3.zero)
            {
                _shotDirection = new Vector3(0f, 0f, -1f);
            }

            _shotDirection = new Vector3(_shotDirection.x, _shotDirection.y + _nextPaintingProjectile.YDirection,
                _shotDirection.z);

            _nextPaintingProjectile.Rigidbody.AddForce(_shotDirection * force,
                ForceMode.Impulse);
        }

        private void PlayShotAnimation()
        {
            _thisAnimator.SetTrigger(ShotAnimationTrigger);
        }

        private void PrepareNextProjectile()
        {
            var targetPool = GetProjectilePool(NextProjectileType);

            _nextPaintingProjectile = targetPool.GetPaintingProjectile();

            ResetMuzzles();
            ActivateMuzzle(_nextPaintingProjectile.ProjectileType);
        }

        private void ActivateMuzzle(PaintingProjectileType projectileType)
        {
            _activeMuzzle = _availableMuzzles.ToList().Find(v => v.PaintingProjectileType == projectileType);

            _activeMuzzle.SetActiveState(true);

            if (!_activeMuzzle)
            {
                CustomDebugLog.ErrorFormat(
                    "[CannonController]: No muzzle for this type of projectile: {0} ... setting random",
                    projectileType);

                _activeMuzzle = _availableMuzzles[0];
            }
        }

        private void ResetMuzzles()
        {
            for (int i = 0; i < _availableMuzzles.Length; i++)
            {
                _availableMuzzles[i].SetActiveState(false);
            }
        }

        private PaintingProjectilePool GetProjectilePool(PaintingProjectileType type)
        {
            var result = ProjectilePools.ToList().Find(v => v.ProjectileType == type);

            if (result == null)
            {
                CustomDebugLog.ErrorFormat("[CannonController]: There is no ProjectilePool of type {0}", type);
            }

            return result;
        }

        private void InitializeProjectilePools(PaintingProjectileType[] availableProjectileTypes)
        {
            ProjectilePools = new PaintingProjectilePool[availableProjectileTypes.Length];

            for (int i = 0; i < ProjectilePools.Length; i++)
            {
                //TODO: prior
                //_projectilePools[i] = new PaintingProjectilePool(availableProjectileTypes[i], _projectilesColor,
                //    _defaultPoolProjectilesCount);

                //TODO: temp
                ProjectilePools[i] = new PaintingProjectilePool(
                    _paintingProjectilePrefabs.ToList().Find(v => v.ProjectileType == availableProjectileTypes[i]),
                    _projectilesColor,
                    DefaultPoolProjectilesCount);
            }
        }

        #region TEMP solution (will be in player input handler)

        private Vector3 _dragStartPos;
        private Vector3 _dragEndPos;

        private bool _dragStarted;

        private Vector3 _aimDirection;

        private void Update()
        {
            if (Input.GetMouseButtonDown(0))
            {
                var screenRay = Camera.main.ScreenPointToRay(Input.mousePosition);

                RaycastHit hit;

                if (Physics.Raycast(screenRay, out hit))
                {
                    if (!hit.collider.CompareTag(CannonTag))
                    {
                        return;
                    }

                    if (!_canShoot)
                    {
                        CustomDebugLog.Message("[CannonController] Reloading...");

                        return;
                    }

                    _dragStarted = true;

                    _dragStartPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                }
            }

            if (Input.GetMouseButton(0) && _dragStarted)
            {
                _aimDirection = _dragStartPos - Camera.main.ScreenToWorldPoint(Input.mousePosition);

                _aimDirection = new Vector3(_aimDirection.x, _aimDirection.y + _nextPaintingProjectile.YDirection,
                    _aimDirection.z);

                _trajectoryDrawer.UpdateTrajectory(_activeMuzzle.ProjectileOutgoingTransform.position,
                    _aimDirection * _nextPaintingProjectile.Speed, new Vector3(0, -9.8f, 0));
            }

            if (Input.GetMouseButtonUp(0) && _dragStarted)
            {
                _dragEndPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

                _shotDirection = -(_dragEndPos - _dragStartPos);

                CustomDebugLog.MessageFormat("[CannonController]: shooting with direction: ", _shotDirection);

                Shoot();

                _dragStarted = false;

                _trajectoryDrawer.Clear();
            }
        }

        private void Rotate(float angle)
        {
            _rotatableTransform.Rotate(Vector3.up, angle);
        }

        #endregion

        private void OnDestroy()
        {
            InkGenerator.Captured -= SignInkGenerator;
        }
    }
}
