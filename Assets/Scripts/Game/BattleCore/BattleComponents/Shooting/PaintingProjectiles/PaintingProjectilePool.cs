﻿using System.Collections.Generic;
using System.Linq;
using Argentics.InkWars.Utils;
using UnityEngine;

namespace Argentics.InkWars.Game.BattleCore.BattleComponents.Shooting.PaintingProjectiles
{
    public class PaintingProjectilePool
    {
        public PaintingProjectileType ProjectileType { get; private set; }

        public List<PaintingProjectile> PaintingProjectiles { get; private set; }

        private readonly Transform _parentTransform;

        private readonly PaintingProjectile _prefab;

        //TODO: temp method
        public PaintingProjectilePool(PaintingProjectile prefab, Color color, int projectilesCount)
        {
            ProjectileType = prefab.ProjectileType;

            _parentTransform = new GameObject("PaintingProjectilePool")
                .GetComponent<Transform>();

            _prefab = prefab;

            InitializePaintingProjectilesPool(projectilesCount, color);
        }

        //TODO: prior method
        public PaintingProjectilePool(PaintingProjectileType projectileType, Color color, int projectilesCount)
        {
            ProjectileType = projectileType;

            _parentTransform = new GameObject("PaintingProjectilePool")
                .GetComponent<Transform>();

            //TODO: load from list of projectiles in SO
            //_prefab = prefab; 

            InitializePaintingProjectilesPool(projectilesCount, color);
        }

        public PaintingProjectile GetPaintingProjectile()
        {
            var result = PaintingProjectiles.ToList().Find(v => !v.gameObject.activeSelf);

            if (result)
            {
                return result;
            }

            result = GameObjectCreator.CreateGameObjectWithComponent(_prefab, _parentTransform);

            PaintingProjectiles.Add(result);

            return result;
        }

        private void InitializePaintingProjectilesPool(int projectilesCount, Color color)
        {
            PaintingProjectiles = new List<PaintingProjectile>();

            for (int i = 0; i < projectilesCount; i++)
            {
                PaintingProjectiles.Add(GameObjectCreator.CreateGameObjectWithComponent(_prefab, _parentTransform));
                     
                PaintingProjectiles[i].Initialize();
                PaintingProjectiles[i].SetColor(color);
                PaintingProjectiles[i].gameObject.SetActive(false);
            }
        }
    }
}
