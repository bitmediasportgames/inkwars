﻿using Argentics.InkWars.Global;
using UnityEngine;

namespace Argentics.InkWars.Game.BattleCore.BattleComponents.Shooting.PaintingProjectiles
{
    //TODO: create scriptable object for painting projectiles

    public enum PaintingProjectileType
    {
        Simple,
        Accurate,
        MegaImpact
    }

    public class PaintingProjectile : Projectile
    {
        public PaintingProjectileType ProjectileType
        {
            get { return _projectileType; }
        }

        //TODO: temp : get the cost from the projectile type list in SO
        public float InkCost
        {
            get { return _inkCost; }
        }

        public float Speed
        {
            get { return _speed; }
        }

        public float YDirection
        {
            get { return _yDirection; }
        }

        protected override ProjectileCollisionData CollisionData
        {
            get { return _collisionData; }
        }

        protected override string CollisionTag
        {
            get { return Globals.Constants.Tags.PaintableTag; }
        }

        [SerializeField] private PaintingProjectileType _projectileType;
        [SerializeField] private float _inkCost = 10f;

        [SerializeField] private float _speed = 10f;
        [SerializeField] private float _yDirection = .05f;

        private string _collisionTag;
        private ProjectileCollisionData _collisionData;

        public override void Initialize()
        {
            base.Initialize();

            _collisionData = new PaintingProjectileCollisionData();
            ((PaintingProjectileCollisionData) _collisionData).InkCost = _inkCost;
            ((PaintingProjectileCollisionData) _collisionData).Type = _projectileType;
        }

        public void SetColor(Color color)
        {
            ((PaintingProjectileCollisionData) _collisionData).Color = color;
        }
    }

    public class PaintingProjectileCollisionData : ProjectileCollisionData
    {
        public PaintingProjectileType Type { get; set; }
        public Color Color { get; set; }
        public float InkCost { get; set; }
    }
}
