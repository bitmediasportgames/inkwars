﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Argentics.InkWars.Game.BattleCore.BattleComponents.Shooting
{
    [Serializable]
    public class ProjectileShotParameters
    {
        public float DamageAreaScale { get; set; }
        public Vector3 Direction { get; set; }
        public float ShotForce { get; set; }
    }

    public abstract class ProjectileCollisionData
    {
        public Vector3 ContactPosition { get; set; }
    }

    [RequireComponent(typeof(Rigidbody), typeof(SphereCollider))]
    public abstract class Projectile : MonoBehaviour
    {
        public event Action<ProjectileCollisionData> Collided;

        protected abstract ProjectileCollisionData CollisionData { get; }

        protected abstract string CollisionTag { get; }

        public Rigidbody Rigidbody { get; protected set; }
        public Transform Transform { get; protected set; }

        public ProjectileShotParameters ShotParameters { get; protected set; }

        public virtual void Initialize()
        {
            InitializeBasics();
        }

        public void PrepareForShot(Transform outgoingTransform)
        {
            Transform.position = outgoingTransform.position;
            Transform.rotation = outgoingTransform.rotation;
        }

        protected bool CollisionIsValid(Collision collision)
        {
            return collision.gameObject.CompareTag(CollisionTag);
        }

        private void InitializeBasics()
        {
            Transform = GetComponent<Transform>();
            Rigidbody = GetComponent<Rigidbody>();

            gameObject.SetActive(false);
        }

        protected void OnCollisionEnter(Collision collision)
        {
            if (!CollisionIsValid(collision))
            {
                return;
            }

            Rigidbody.velocity = Vector3.zero;

            gameObject.SetActive(false);

            if (Collided != null)
            {
                RaycastHit hit;

                if (Physics.Raycast(collision.contacts[0].point, new Vector3(0, -1, 0), out hit))
                {
                    CollisionData.ContactPosition = hit.point;
                }

                //CollisionData.ContactPosition = collision.contacts[0].point;

                Collided(CollisionData);
            }
        }
    }
}
