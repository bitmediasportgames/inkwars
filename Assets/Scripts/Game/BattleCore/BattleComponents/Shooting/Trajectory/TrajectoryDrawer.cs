﻿using UnityEngine;

namespace Argentics.InkWars.Game.BattleCore.BattleComponents.Shooting.Trajectory
{
    [RequireComponent(typeof(LineRenderer))]
    public class TrajectoryDrawer : MonoBehaviour
    {
        private LineRenderer _lineRenderer;

        private const int _stepsNumber = 50;

        public void Initialize()
        {
            _lineRenderer = GetComponent<LineRenderer>();

            Clear();
        }

        public void UpdateTrajectory(Vector3 initialPosition, Vector3 initialVelocity, Vector3 gravity)
        {
            float timeDelta = 1.0f / initialVelocity.magnitude; // for example

            _lineRenderer.positionCount = _stepsNumber;

            Vector3 position = initialPosition;
            Vector3 velocity = initialVelocity;

            for (int i = 0; i < _stepsNumber; ++i)
            {
                _lineRenderer.SetPosition(i, position);

                position += velocity * timeDelta + 0.5f * gravity * timeDelta * timeDelta;
                velocity += gravity * timeDelta;
            }
        }

        public void Clear()
        {
            _lineRenderer.positionCount = 0;
        }
    }
}
