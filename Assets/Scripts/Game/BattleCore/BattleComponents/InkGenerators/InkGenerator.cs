﻿using System;
using System.Collections;
using Argentics.InkWars.Game.BattleCore.BattleComponents.Soldiers;
using Argentics.InkWars.Global;
using CustomDebugDll;
using UnityEngine;

namespace Argentics.InkWars.Game.BattleCore.BattleComponents.InkGenerators
{
    [Serializable]
    public class InkGeneratorCapturedData
    {
        public int InkGeneratorId;
        public float InkMultiplyValue;
    }

    [RequireComponent(typeof(Collider), typeof(Animator))]
    public class InkGenerator : MonoBehaviour
    {
        public static event Action<InkGeneratorCapturedData> Captured;
        public static int CapturedCount = 0;
        public static int TotalCount = 0;

        [SerializeField] private InkGeneratorCapturedData _inkGeneratorData;
        [SerializeField] private Renderer _mainRenderer;
        [SerializeField] private Renderer[] _capsuleMeshRenderers;

        private Animator _animator;
        private Collider _collider;

        private int _currentOwnerId;
        private Color _currentInkColor;

        private bool _isCaptured;

        private bool _collisionInProgress;

        private const string CapturingAnimationTrigger = "capturedTrigger";

        private void Start()
        {
            Initialize();
        }

        public void Initialize()
        {
            TotalCount++;

            _animator = GetComponent<Animator>();
            _collider = GetComponent<Collider>();

            StartCoroutine(BlinkingCoroutine());
        }

        private IEnumerator OnTriggerEnter(Collider collision)
        {
            if (_collisionInProgress)
            {
                yield break;
            }
            else
            {
                _collisionInProgress = true;
            }

            if (!collision.gameObject.CompareTag(Globals.Constants.Tags.StickmanTag))
            {
                _collisionInProgress = false;

                yield break;;
            }

            var soldierData = collision.gameObject.GetComponent<Stickman>();

            if (!soldierData)
            {
                CustomDebugLog.Warning("[InkGenerator]: No Stick component on the target!");

                _collisionInProgress = false;

                yield break;
            }

            if (_isCaptured && _currentOwnerId == soldierData.OwnerId)
            {
                _collisionInProgress = false;

                yield break;
            }

            //TODO: run animation e.g.

            yield return new WaitForSeconds(Globals.Constants.BattleParameters.InkGeneratorCaptureTime);

            Capture(soldierData.OwnerId, soldierData.Color);

            _collisionInProgress = false;
        }

        private IEnumerator BlinkingCoroutine()
        {
            while (!_isCaptured)
            {
                _mainRenderer.materials[0].color = Color.white;

                yield return new WaitForSeconds(1f);

                _mainRenderer.materials[0].color = Color.red;

                yield return new WaitForSeconds(1f);
            }
        }
        
        private void Capture(int actorId, Color playerColor)
        {
            _animator.SetTrigger(CapturingAnimationTrigger);

            _currentOwnerId = actorId;
            _currentInkColor = playerColor;

            ChangeColor(playerColor);

            CapturedCount++;

            _isCaptured = true;

            if (Captured != null)
            {
                Captured(_inkGeneratorData);
            }

            _collider.enabled = false;

            StopCoroutine(BlinkingCoroutine());

            CustomDebugLog.Message("[InkGenerator]: Captured!");
        }

        private void ChangeColor(Color color)
        {
            for (int i = 0; i < _capsuleMeshRenderers.Length; i++)
            {
                _capsuleMeshRenderers[i].gameObject.SetActive(true);
                _capsuleMeshRenderers[i].material.color = color;
            }
        }
    }
}


