﻿using UnityEngine;
using UnityEngine.AI;

namespace Argentics.InkWars.Game.BattleCore.BattleComponents.Soldiers
{
    [RequireComponent(typeof(NavMeshAgent), typeof(Animator))]
    public class Stickman : MonoBehaviour
    {
        public int OwnerId { get; private set; }
        public Color Color { get; private set; }

        private NavMeshAgent _thisNavMeshAgent;
        private Animator _thisAnimator;
        private Rigidbody _thisRigidbody;

        private const string RunStateValue = "runValue";

        public void Initialize(int actorId, Color color)
        {
            _thisNavMeshAgent = GetComponent<NavMeshAgent>();
            _thisAnimator = GetComponent<Animator>();
            _thisRigidbody = GetComponent<Rigidbody>();

            OwnerId = actorId;

            Color = color;
        }

        public void SendToPoint(Vector3 point)
        {
            _thisNavMeshAgent.destination = point;
        }

        private void Update()
        {
            _thisAnimator.SetInteger(RunStateValue, _thisRigidbody.IsSleeping() ? 0 : 1);
        }
    }
}
