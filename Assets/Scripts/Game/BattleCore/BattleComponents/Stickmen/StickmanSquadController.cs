﻿using Argentics.InkWars.Game.BattleCore.BattleComponents.InkGenerators;
using Argentics.InkWars.Game.BattleCore.Environment;
using Argentics.InkWars.Global;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Argentics.InkWars.Game.BattleCore.BattleComponents.Soldiers
{
    //TODO: instantiate stickman prefab by number
    public class StickmanSquadController : MonoBehaviour
    {
        [SerializeField] private Stickman[] _stickmen;
        [SerializeField] private Transform _spawnPoint;

        private int _ownerId;

        private Color _color;

        private Transform _thisTransform;

        private BattleLevelPainter _battleLevelPainter;

        private string _inkGeneratorTag = Globals.Constants.Tags.InkGeneratorTag;

        private Camera _mainCamera;

        public void Initialize(int actorId, Color color)
        {
            InkGenerator.Captured += SendSquadBack;

            _thisTransform = GetComponent<Transform>();

            _thisTransform.position = _spawnPoint.position;

            _mainCamera = Camera.main;

            _ownerId = actorId;
            _color = color;

            for (int i = 0; i < _stickmen.Length; i++)
            {
                _stickmen[i].Initialize(_ownerId, _color);
            }

            if (!gameObject.activeSelf)
            {
                gameObject.SetActive(true);
            }
        }

        public void SendSquad(Vector3 targetPosition)
        {
            for (int i = 0; i < _stickmen.Length; i++)
            {
                _stickmen[i].SendToPoint(targetPosition);
            }
        }

        private void SendSquadBack(InkGeneratorCapturedData data)
        {
            SendSquad(_spawnPoint.position);
        }

        //TODO: temp : move to input handler
        private void Update()
        {
            if (Input.GetMouseButton(0))
            {
                ProceedSquadSendingRequest();
            }
        }

        private void ProceedSquadSendingRequest()
        {
            //TODO: use a direct reference
            var screenRay = Camera.main.ScreenPointToRay(Input.mousePosition);

            RaycastHit hit;

            if (Physics.Raycast(screenRay, out hit))
            {
                if (!hit.collider.CompareTag(_inkGeneratorTag))
                {
                    return;
                }

                SendSquad(hit.point + Random.insideUnitSphere);
            }
        }

        private void OnDestroy()
        {
            InkGenerator.Captured -= SendSquadBack;
        }
    }
}
