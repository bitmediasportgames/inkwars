﻿using Argentics.InkWars.BattleCore.Additive;
using Argentics.InkWars.Game.BattleCore.BattleCamera;
using Argentics.InkWars.Game.BattleCore.BattleComponents.Shooting;
using Argentics.InkWars.Game.BattleCore.BattleComponents.Soldiers;
using Argentics.InkWars.Game.BattleCore.Environment;
using Argentics.InkWars.Game.BattleCore.InputHandling;
using Argentics.InkWars.Game.Player;
using CustomDebugDll;
using Es.InkPainter;

namespace Argentics.InkWars.Game.BattleCore.BattleActors
{
    public class BattleActorData
    {
        public PlayerData PlayerData { get; set; }
        public CannonController CannonController { get; set; }
        public StickmanSquadController SquadController { get; set; }
        public BattleCameraController CameraController { get; set; }
        public InputHandler InputHandler { get; set; }
        public InkCanvas TargetInkCanvas { get; set; }
        public PaintingProjectileBrush[] AvailableBrushes { get; set; }
    }

    public class BattleActor
    {
        public static int Id = -1;

        public InputHandler InputHandler { get; private set; }
        public CannonController CannonController { get; private set; }

        private StickmanSquadController _squadController;
        private BattleCameraController _cameraController;

        private readonly BattleActorData _actorData;

        public BattleActor(BattleActorData actorData)
        {
            Id++;

            _actorData = actorData;

            InitializeInputHandler();
            InitializeCannonController();
            InitializeSquadController();

            if (_actorData.PlayerData.ThisPlayer)
            {
                InitializeCameraController();
            }
            
            SignInputHandler();
        }

        public void Update()
        {
            InputHandler.Update();
        }

        private void SignInputHandler()
        {
            InputHandler.SimplePaintRequested += CannonController.OnSimpleRequested;
            InputHandler.AccuratePaintRequested += CannonController.OnAccurateRequested;
            InputHandler.MegaImpactPaintRequested += CannonController.OnMegaRequested;
            //TODO: sign camera controller
        }

        private void UnsignInputHandler()
        {
            InputHandler.ShotRequested -= CannonController.Shoot;
        }
        
        private void InitializeInputHandler()
        {
            InputHandler = _actorData.InputHandler;
        }

        private void InitializeCannonController()
        {
            CannonController = _actorData.CannonController;

            var levelPainter = new BattleLevelPainter(_actorData.TargetInkCanvas, _actorData.AvailableBrushes);

            CannonController.Initialize(_actorData.PlayerData, levelPainter);
        }

        private void InitializeSquadController()
        {
            _squadController = _actorData.SquadController;

            if (!_squadController)
            {
                CustomDebugLog.Warning("[BattleActor]: SquadController is not initialized or null");

                return;
            }

            _squadController.Initialize(0, _actorData.PlayerData.Color);
        }

        private void InitializeCameraController()
        {
            _cameraController = _actorData.CameraController;

            _cameraController.Initialize();
        }
    }
}
