﻿using System;
using Es.InkPainter;
using UnityEngine;

namespace Argentics.InkWars.Game.BattleCore.Environment
{
    //TODO: implement with serializable objects

    [Serializable]
    public class BattleLevelStorage
    {
        public BattleLevel[] BattleLevels
        {
            get { return _battleLevels; }
        }

        [SerializeField] private BattleLevel[] _battleLevels;
    }

    public class BattleLevel : MonoBehaviour
    {
        public int Id
        {
            get { return _id; }
        }

        public Transform RootTransform
        {
            get { return _rootTransform; }
        }

        public Transform TopCannonTransform
        {
            get { return _topCannonTransform; }
        }

        public Transform DownCannonTransform
        {
            get { return _downCannonTransform; }
        }

        public InkCanvas InkCanvas
        {
            get { return _inkCanvas; }
        }

        public Transform SquadSpawnTransform
        {
            get { return _squadSpawnTransform; }
        }

        [SerializeField] private int _id;

        [SerializeField] private Transform _rootTransform;

        [SerializeField] private Transform _topCannonTransform;
        [SerializeField] private Transform _downCannonTransform;

        [SerializeField] private InkCanvas _inkCanvas;

        [SerializeField] private Transform _squadSpawnTransform;

        public void Initialize()
        {
            if (!gameObject.activeSelf)
            {
                gameObject.SetActive(true);
            }
        }
    }
}
