﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Argentics.InkWars.BattleCore.Additive;
using Argentics.InkWars.Game.BattleCore.BattleComponents.Shooting;
using Argentics.InkWars.Game.BattleCore.BattleComponents.Shooting.PaintingProjectiles;
using Argentics.InkWars.Global;
using Argentics.InkWars.Utils;
using CustomDebugDll;
using Es.InkPainter;
using UnityEngine;

namespace Argentics.InkWars.Game.BattleCore.Environment
{
    public class BattleLevelPainter
    {
        public event Action<float> InkValueChanged; 

        private PaintingProjectileBrush[] _availableBrushes;
        private InkCanvas _inkCanvas;

        private float _availableInkAmount;
        private float _defaultInkAmount;
        private float _inkRestoringSpeed;
        private float _restoringTimePeriod = 1f;

        private Coroutine _inkRestoringCoroutine;

        private CoroutineRunner _coroutineRunner;

        public BattleLevelPainter(InkCanvas inkCanvas, PaintingProjectileBrush[] brushes)
        {
            _inkCanvas = inkCanvas;
            _availableBrushes = brushes;
            _defaultInkAmount = Globals.Constants.BattleParameters.DefaultActorInkAmount;
            _inkRestoringSpeed = Globals.Constants.BattleParameters.DefaultInkRestoringSpeed;
            _availableInkAmount = _defaultInkAmount;

            if (InkValueChanged != null)
            {
                InkValueChanged(_availableInkAmount);
            }

            _coroutineRunner = new GameObject("CoroutineRunner").AddComponent<CoroutineRunner>();

            StartInkRestoring();

            CustomDebugLog.Message("[BattleLevelPainter]: BattleLevelPainter created!");
        }

        public void AddProjectileSubjects(IList<PaintingProjectile> projectiles)
        {
            for (int i = 0; i < projectiles.Count; i++)
            {
                projectiles[i].Collided += OnPaintingProjectileCollided;
            }

            CustomDebugLog.Message("[BattleLevelPainter]: Signed projectile collision events!");
        }

        public void AddSubjects(IList<Action<ProjectileCollisionData>> projectiles)
        {
            for (int i = 0; i < projectiles.Count; i++)
            {
                projectiles[i] += OnPaintingProjectileCollided;
            }

            CustomDebugLog.Message("[BattleLevelPainter]: Signed projectile collision events!");
        }

        public void IncreaseInkRestoringSpeed(float value)
        {
            _inkRestoringSpeed += value;
        }

        public bool EnoughInk(float inkCost)
        {
            return _availableInkAmount - inkCost > 0;
        }

        private void OnPaintingProjectileCollided(ProjectileCollisionData data)
        {
            var convertedData = (PaintingProjectileCollisionData) data;

            if (convertedData == null)
            {
                CustomDebugLog.Error("[BattleLevelPainter]: Cannot cast projectile collision data!");

                return;
            }

            var targetBrush = _availableBrushes.ToList().Find(v => v.PaintingProjectileType == convertedData.Type);

            CustomDebugLog.MessageFormat("[BattleLevelPainter]: Converted projectile collision data type: {0}",
                convertedData.Type);

            if (targetBrush == null)
            {
                CustomDebugLog.Error("[BattleLevelPainter]: Cannot cast projectile collision data!");

                return;
            }

            targetBrush.Brush.Color = convertedData.Color;

            Paint(targetBrush.Brush, convertedData.ContactPosition, convertedData.InkCost);

            CustomDebugLog.MessageFormat("[BattleLevelPainter]: Painting on position: {0}",
                convertedData.ContactPosition);
        }

        private void Paint(Brush brush, Vector3 position, float inkCost)
        {
            _inkCanvas.Paint(brush, position);

            _availableInkAmount -= inkCost;

            if (InkValueChanged != null)
            {
                InkValueChanged(_availableInkAmount);
            }

            CustomDebugLog.MessageFormat("[BattleLevelPainter]: AvailableInkAmount: {0}", _availableInkAmount);
        }

        private void StartInkRestoring()
        {
            _inkRestoringCoroutine = _coroutineRunner.RunCoroutine(InkRestoringCoroutine());
        }

        private void StopInkRestoring()
        {
            _coroutineRunner.StopCoroutine(_inkRestoringCoroutine);
        }

        private IEnumerator InkRestoringCoroutine()
        {
            while (true)
            {
                yield return new WaitForSeconds(_restoringTimePeriod);

                if (_availableInkAmount + _inkRestoringSpeed > _defaultInkAmount)
                {
                    _availableInkAmount = _defaultInkAmount;
                }
                else
                {
                    _availableInkAmount += _inkRestoringSpeed;
                }

                if (InkValueChanged != null)
                {
                    InkValueChanged(_availableInkAmount);
                }
            }
        }
    }
}
