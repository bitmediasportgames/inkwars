﻿using System;
using Argentics.InkWars.Game.BattleCore.BattleComponents.Shooting.PaintingProjectiles;
using Es.InkPainter;
using UnityEngine;

namespace Argentics.InkWars.BattleCore.Additive
{
    [Serializable]
    public class PaintingProjectileBrush
    {
        public PaintingProjectileType PaintingProjectileType
        {
            get { return _paintingProjectileType; }
        }

        public Brush Brush
        {
            get { return _brush; }
        }

        [SerializeField] private PaintingProjectileType _paintingProjectileType;
        [SerializeField] private Brush _brush;
    }
}
