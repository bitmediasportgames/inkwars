﻿using System;
using System.Collections;
using Argentics.InkWars.Game.BattleCore.BattleComponents.InkGenerators;
using UnityEngine;

namespace Argentics.InkWars.Game.BattleCore.BattleJudgment
{
    //TODO: take in constructor references to ink generators that are stored in battle level class
    //или передавать сюда двух представителей участников, которые хранят в себе данные о количестве закраски и генераторов 
    //у каждого игрока
    public class BattleJudgmentSystem
    {
        public event Action AllInkGeneratorsCaptured;
        public event Action PaintedLevelPercentageReached;
        public event Action TimeIsOut; 

        private float _battleTime;
        private float _levelPercentage;

        private BattleConfig _battleConfig;

        private bool _isActive;

        public BattleJudgmentSystem(BattleConfig battleConfig)
        {
            _battleConfig = battleConfig;

            _battleTime = battleConfig.Time;

            switch (battleConfig.BattleType)
            {
                case BattleType.GeneratorsCapturing:
                    InkGenerator.Captured += OnInkGeneratorCaptured;
                    break;
                case BattleType.LevelPainting:
                    _levelPercentage = battleConfig.LevelPercentage;
                    //активировать счетчик краски
                    break;

            }
        }

        private void OnInkGeneratorCaptured(InkGeneratorCapturedData capturedData)
        {
            if (InkGenerator.CapturedCount == InkGenerator.TotalCount)
            {
                _isActive = false;

                if (AllInkGeneratorsCaptured != null)
                {
                    AllInkGeneratorsCaptured();
                }
            }
        }

        private IEnumerator JudgmentCoroutine()
        {
            while (_battleTime > 0)
            {
                yield return new WaitForSecondsRealtime(1f);

                _battleTime--;
            }

            if (TimeIsOut != null)
            {
                TimeIsOut();
            }
        }
    }
}
