﻿using System;
using System.Linq;
using Argentics.InkWars.BattleCore.Additive;
using Argentics.InkWars.BattleCore.InputHandling.InputHandlers;
using Argentics.InkWars.Game.BattleCore.BattleActors;
using Argentics.InkWars.Game.BattleCore.BattleCamera;
using Argentics.InkWars.Game.BattleCore.BattleComponents.InkGenerators;
using Argentics.InkWars.Game.BattleCore.BattleComponents.Shooting;
using Argentics.InkWars.Game.BattleCore.BattleComponents.Shooting.PaintingProjectiles;
using Argentics.InkWars.Game.BattleCore.BattleComponents.Soldiers;
using Argentics.InkWars.Game.BattleCore.BattleJudgment;
using Argentics.InkWars.Game.BattleCore.Environment;
using Argentics.InkWars.Game.BattleCore.InputHandling;
using Argentics.InkWars.Game.BattleCore.InputHandling.InputHandlers;
using Argentics.InkWars.Game.Player;
using Argentics.InkWars.Utils;
using CustomDebugDll;
using Es.InkPainter;
using UnityEngine;
using UnityEngine.UI;

namespace Argentics.InkWars.Game.BattleCore
{
    public class BattleManager : MonoBehaviour
    {
        public event Action<BattleResultData> BattleEnded;

        public Color[] AvailableColors
        {
            get { return _availableColors; }
        }

        [Space, Header("TEMP")]
        [SerializeField] private Text _currentInkValue;
        [SerializeField] private Text _currentGenValue; 

        //TODO: temp; replace with SO
        [Space, Header("SO objects")]
        [SerializeField] private CannonController _cannonController;
        [SerializeField] private StickmanSquadController _squadControllerPrefab;
        [SerializeField] private BattleCameraController _cameraControllerPrefab;
        [SerializeField] private PaintingProjectileBrush[] _brushes;
        [SerializeField] private BattleLevelStorage _levelStorage;
        [SerializeField] private PlayerInputRaiser _playerInputRaiser;

        [SerializeField] private Color[] _availableColors;
        //TODO: temp; replace with SO

        [Space, Header("Editor only"), SerializeField]
        private ApplicationManager _reserveAppManagerPrefab;
        [SerializeField] private bool _debug = true;

        private BattleJudgmentSystem _judgmentSystem;

        private InkCanvas _targetInkCanvas;

        private BattleActor _thisPlayerActor;
        private BattleActor _oppositePlayerActor;

        private BattleConfig _battleConfig;

        private BattleLevel _level;

        private bool _isReady;

        private void Start()
        {
            if (_debug)
            {
                RunDefaultInitialization();
            }
        }

        private void UpdateInkAmount(float value)
        {
            _currentInkValue.text = string.Format("INK: {0}", (int)value);
        }

        private void UpdateGeneratorsAmount(InkGeneratorCapturedData data)
        {
            _currentGenValue.text = string.Format("GENS: {0}", InkGenerator.CapturedCount);
        }

        private void RunDefaultInitialization()
        {
            var thisPlayerData = new PlayerData
            {
                AvailableProjectileTypes = new[]
                    {PaintingProjectileType.Simple, PaintingProjectileType.Accurate, PaintingProjectileType.MegaImpact},
                Color = _availableColors[0],
                Name = "Bilbo",
                ThisPlayer = true
            };

            var oppositePlayerData = new PlayerData
            {
                AvailableProjectileTypes = new[]
                    {PaintingProjectileType.Simple, PaintingProjectileType.Accurate, PaintingProjectileType.MegaImpact},
                Color = _availableColors[1],
                Name = "Lalana",
                ThisPlayer = false
            };

            var battleConfig = new BattleConfig
            {
                BattleMode = BattleMode.SinglePlayer,
                BattleType = BattleType.LevelPainting,
                LevelId = 1,
                ThisPlayerData = thisPlayerData,
                OppositePlayerData = oppositePlayerData
            };

            Initialize(battleConfig);
        }

        public void Initialize(BattleConfig battleConfig)
        {
            _battleConfig = battleConfig;

            BuildLevel();

            _targetInkCanvas = _level.InkCanvas;

            CreateActors();

            _isReady = true;

            //TODO: super temp
            _thisPlayerActor.CannonController.LevelPainter.InkValueChanged += UpdateInkAmount;
            InkGenerator.Captured += UpdateGeneratorsAmount;

            StartBattle();
        }

        public void StartBattle()
        {

        }

        public void FinishBattle()
        {
        }

        private void Update()
        {
            if (!_isReady)
            {
                return;
            }

            _thisPlayerActor.Update();
            _oppositePlayerActor.Update();
        }

        private void BuildLevel()
        {
            var targetLevelPrefab = _levelStorage.BattleLevels.ToList().Find(v => v.Id == _battleConfig.LevelId);

            _level = GameObjectCreator.CreateGameObjectWithComponent(targetLevelPrefab);

            _level.Initialize();
        }

        private void CreateActors()
        {
            InputHandler oppositeInputHandler;

            if (_battleConfig.BattleMode == BattleMode.MultiPlayer)
            {
                oppositeInputHandler = new NetworkInputHandler(_playerInputRaiser);
            }
            else
            {
                oppositeInputHandler = new AIInputHandler(_playerInputRaiser);
            }

            _thisPlayerActor =
                new BattleActor(new BattleActorData
                {
                    PlayerData = _battleConfig.ThisPlayerData, TargetInkCanvas = _targetInkCanvas,
                    CannonController = GameObjectCreator.CreateGameObjectWithComponent(_cannonController,
                        _level.DownCannonTransform.position, _level.DownCannonTransform.rotation),
                    CameraController = GameObjectCreator.CreateGameObjectWithComponent(_cameraControllerPrefab),
                    SquadController = GameObjectCreator.CreateGameObjectWithComponent(_squadControllerPrefab),
                    AvailableBrushes = _brushes,
                    InputHandler = new PlayerInputHandler(_playerInputRaiser)
                });

            _oppositePlayerActor =
                new BattleActor(new BattleActorData
                {
                    PlayerData = _battleConfig.OppositePlayerData,
                    TargetInkCanvas = _targetInkCanvas,
                    CannonController = GameObjectCreator.CreateGameObjectWithComponent(_cannonController,
                        _level.TopCannonTransform.position, _level.TopCannonTransform.rotation),
                    //SquadController = GameObjectCreator.CreateGameObjectWithComponent(_squadControllerPrefab),
                    AvailableBrushes = _brushes,
                    InputHandler = oppositeInputHandler,
                });

            CustomDebugLog.MessageFormat(this, "Battle Actors were created!");
        }
    }

    public class BattleConfig
    {
        public PlayerData ThisPlayerData { get; set; }
        public PlayerData OppositePlayerData { get; set; }

        public BattleMode BattleMode { get; set; }
        public BattleType BattleType { get; set; }

        public int LevelId { get; set; }

        public float Time { get; set; }
        public float LevelPercentage { get; set; }
    }

    public enum BattleMode
    {
        SinglePlayer, 
        MultiPlayer
    }

    public enum BattleType
    {
        LevelPainting,
        GeneratorsCapturing
    }

    public class BattleResultData
    {

    }
}
