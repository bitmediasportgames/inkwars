﻿using System;
using UnityEngine;

namespace Argentics.InkWars.Game.BattleCore.InputHandling
{
    public abstract class InputHandler
    {
        public event Action ShotRequested;

        public event Action SimplePaintRequested;
        public event Action AccuratePaintRequested;
        public event Action MegaImpactPaintRequested;

        public event Action<int> SquadRequested;
        public event Action<Vector2> CameraScrollRequested;
        public event Action<float> CameraZoomRequested;

        public abstract void Update();

        protected InputHandler(IInputRaiser inputRaiser)
        {
            inputRaiser.SignSimplePaintRequest(RequestSimplePaint);
            inputRaiser.SignAccuratePaintRequest(RequestAccuratePaint);
            inputRaiser.SignMegaImpactPaintRequest(RequestMegaImpactPaint);
        }

        protected void RequestShot()
        {
            if (ShotRequested != null)
            {
                ShotRequested();
            }
        }

        protected void RequestSimplePaint()
        {
            if (SimplePaintRequested != null)
            {
                SimplePaintRequested();
            }
        }

        protected void RequestAccuratePaint()
        {
            if (AccuratePaintRequested != null)
            {
                AccuratePaintRequested();
            }
        }

        protected void RequestMegaImpactPaint()
        {
            if (MegaImpactPaintRequested != null)
            {
                MegaImpactPaintRequested();
            }
        }

        protected void RequestSquad(int targetId)
        {
            if (SquadRequested != null)
            {
                SquadRequested(targetId);
            }
        }

        protected void RequestCameraScroll(Vector2 targetPosition)
        {
            if (CameraScrollRequested != null)
            {
                CameraScrollRequested(targetPosition);
            }
        }

        protected void RequestCameraZoom(float delta)
        {
            if (CameraZoomRequested != null)
            {
                CameraZoomRequested(delta);
            }
        }
    }
}
