﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Argentics.InkWars.Game.BattleCore.InputHandling
{
    [Serializable]
    public class PlayerInputRaiser : IInputRaiser
    {
        [SerializeField] private Button _thisPlayerShotButton;

        [SerializeField] private Button _simplePaintButton;
        [SerializeField] private Button _accuratePaintButton;
        [SerializeField] private Button _megaImpactPaintButton;

        public void SignShotRequest(Action targetAction)
        {
            _thisPlayerShotButton.onClick.AddListener(delegate { targetAction(); });
        }

        public void SignSimplePaintRequest(Action targetAction)
        {
            _simplePaintButton.onClick.AddListener(delegate { targetAction(); });
        }

        public void SignAccuratePaintRequest(Action targetAction)
        {
            _accuratePaintButton.onClick.AddListener(delegate { targetAction(); });
        }

        public void SignMegaImpactPaintRequest(Action targetAction)
        {
            _megaImpactPaintButton.onClick.AddListener(delegate { targetAction(); });
        }
    }
}
