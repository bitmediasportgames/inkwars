﻿using System;

namespace Argentics.InkWars.Game.BattleCore.InputHandling
{
    public interface IInputRaiser
    {
        void SignShotRequest(Action targetAction);
        void SignSimplePaintRequest(Action targetAction);
        void SignAccuratePaintRequest(Action targetAction);
        void SignMegaImpactPaintRequest(Action targetAction);
    }
}
