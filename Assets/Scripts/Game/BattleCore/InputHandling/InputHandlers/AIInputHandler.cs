﻿using Argentics.InkWars.Game.BattleCore.InputHandling;
using CustomDebugDll;

namespace Argentics.InkWars.BattleCore.InputHandling.InputHandlers
{
    public class AIInputHandler : InputHandler
    {
        public AIInputHandler(IInputRaiser inputRaiser) : base(inputRaiser)
        {
            CustomDebugLog.Message("AIInputHandler was initialized!");
        }

        public override void Update()
        {
            
        }
    }
}
