﻿using CustomDebugDll;
using UnityEngine;

namespace Argentics.InkWars.Game.BattleCore.InputHandling.InputHandlers
{
    public class PlayerInputHandler : InputHandler
    {
        private Camera _thisCamera;

        private Vector3 _touchStart;

        public PlayerInputHandler(IInputRaiser inputRaiser) : base(inputRaiser)
        {
            _thisCamera = Camera.main;

            CustomDebugLog.Message("PlayerInputHandler was initialized!");
        }

        public override void Update()
        {
            //if (Input.GetMouseButtonDown(0))
            //{
            //    _touchStart = _thisCamera.ScreenToWorldPoint(Input.mousePosition);
            //}
            //if (Input.touchCount == 2)
            //{
            //    var touchZero = Input.GetTouch(0);
            //    var touchOne = Input.GetTouch(1);

            //    var touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
            //    var touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

            //    var prevMagnitude = (touchZeroPrevPos - touchOnePrevPos).magnitude;
            //    var currentMagnitude = (touchZero.position - touchOne.position).magnitude;

            //    var difference = currentMagnitude - prevMagnitude;

            //    RequestCameraZoom(difference * 0.01f);
            //}
            //else if (Input.GetMouseButton(0))
            //{
            //    var direction = _touchStart - _thisCamera.ScreenToWorldPoint(Input.mousePosition);

            //    _thisCamera.transform.position += direction;
            //}

            //RequestCameraZoom(Input.GetAxis("Mouse ScrollWheel"));
        }
    }
}
