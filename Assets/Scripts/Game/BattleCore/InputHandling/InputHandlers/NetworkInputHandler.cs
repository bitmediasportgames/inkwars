﻿namespace Argentics.InkWars.Game.BattleCore.InputHandling.InputHandlers
{
    public class NetworkInputHandler : InputHandler
    {
        public NetworkInputHandler(IInputRaiser inputRaiser) : base(inputRaiser)
        {
        }

        public override void Update()
        {
        }
    }
}
