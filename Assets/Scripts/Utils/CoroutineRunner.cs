﻿using System.Collections;
using UnityEngine;

namespace Argentics.InkWars.Utils
{
    public class CoroutineRunner : MonoBehaviour
    {
        public Coroutine RunCoroutine(IEnumerator targetRoutine)
        {
            return StartCoroutine(targetRoutine);
        }

        public void BreakCoroutine(IEnumerator targetRoutine)
        {
            BreakCoroutine(targetRoutine);
        }
    }
}
