﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Argentics.Utils
{
    public static class CommonUtils
    {
        public static DateTime UnixTimeStampToUtcDateTime(long unixTimeStamp)
        {
            // Unix timestamp is seconds past epoch
            DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(unixTimeStamp);
            return dtDateTime;
        }

        public static long UtcDateTimeToUnixTimeStamp(DateTime dateTime)
        {
            //var specifiedTime = DateTime.SpecifyKind(dateTime, DateTimeKind.Local);
            //var utcDateTime = TimeZoneInfo.ConvertTimeToUtc(specifiedTime);
            long unixTimestamp = (long)(dateTime.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
            return unixTimestamp;
        }

        public static T StringToEnum<T>(string str) where T : struct
        {
            try
            {
                T res = (T)Enum.Parse(typeof(T), str);
                if (!Enum.IsDefined(typeof(T), res)) return default(T);
                return res;
            }
            catch
            {
                Debug.Log(new Exception("No " + str + " in " + typeof(T).ToString() + "enum"));
                return default(T);
            }
        }
    }
}