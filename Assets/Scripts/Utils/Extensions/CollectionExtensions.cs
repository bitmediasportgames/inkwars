﻿using System.Collections;
using System.Collections.Generic;

namespace Argentics.Utils.Extensions
{
    public static class CollectionExtensions
    {
        public static void Add(this Dictionary<string, float> origin, Dictionary<string,float> added)
        {
            IEnumerator keysEnumerator = added.Keys.GetEnumerator();
            while(keysEnumerator.MoveNext())
            {
                string key = (string)keysEnumerator.Current;
                if (origin.ContainsKey(key))
                    origin[key] += added[key];
                else
                    origin.Add(key, added[key]);
            }
        }
    }
}