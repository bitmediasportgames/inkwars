﻿using UnityEngine;

namespace Argentics.InkWars.Utils
{
    public class GameObjectCreator : MonoBehaviour 
    {
        public static GameObject CreateGameObject(GameObject component)
        {
            return Instantiate(component);
        }

        public static T CreateGameObjectWithComponent<T>(T component) where T : Component
        {
            return Instantiate(component);
        }

        public static T CreateGameObjectWithComponent<T>(T component, Transform parent) where T : Component
        {
            return Instantiate(component, parent, false) as T;
        }

        public static T CreateGameObjectWithComponent<T>(T component, Vector3 position, Quaternion rotation) where T : Component
        {
            return Instantiate(component, position, rotation) as T;
        }
    }
}
