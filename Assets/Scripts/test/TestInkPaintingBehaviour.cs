﻿using System.Collections;
using System.Linq;
using Argentics.InkWars.Game.BattleCore.BattleComponents.Shooting;
using Argentics.InkWars.Game.BattleCore.BattleComponents.Shooting.PaintingProjectiles;
using Es.InkPainter;
using UnityEngine;

namespace Assets.Scripts.Temp
{
    public class TestInkPaintingBehaviour : MonoBehaviour
    {
        [SerializeField] private Transform _playerTurret;
        [SerializeField] private Transform _enemyTurret;

        [SerializeField] private PaintingProjectile _projectilePrefab;

        [SerializeField] private int _iterationsCount = 300;

        private PaintingProjectile[] _projectilePool = new PaintingProjectile[ProjectilePoolCapacity];

        private const int ProjectilePoolCapacity = 20;

        private void Awake()
        {
            InitializePool();

            //RunTestCondition();
        }

        private void InitializePool()
        {
            for (int i = 0; i < ProjectilePoolCapacity; i++)
            {
                _projectilePool[i] = Instantiate(_projectilePrefab);

                _projectilePool[i].SetColor(Color.green);
            }
        }

        private void RunTestCondition()
        {
            StartCoroutine(TestConditionCoroutine());
        }

        private IEnumerator TestConditionCoroutine()
        {
            for (int i = 0; i < _iterationsCount; i++)
            {
                DoPlayerShot();
                DoEnemyShot();

                yield return new WaitForSeconds(1f);
            }
        }

        public void DoPlayerShot()
        {
            DoShot(_playerTurret, true);
        }

        public void DoEnemyShot()
        {
            DoShot(_enemyTurret, false);
        }

        private void DoShot(Transform outgoingTransform, bool isPlayer)
        {
            var projectile = GetProjectile();

            projectile.gameObject.SetActive(true);

            projectile.transform.position = outgoingTransform.position;
            projectile.transform.rotation = outgoingTransform.rotation;

            var randomDirection = new Vector3(Random.Range(-.5f, .5f), 0f, 0f);
            var force = Random.Range(10f, 60f);

            projectile.Rigidbody.AddForce((new Vector3(0f, 0f, isPlayer ? 1f : -1f) + randomDirection) * force, ForceMode.Impulse);
        }

        private Projectile GetProjectile()
        {
            //TODO: add items in pool in case it's empty

            return _projectilePool.ToList().Find(v => !v.gameObject.activeSelf);
        }

        public void OnGUI()
        {
            if (GUILayout.Button("Reset"))
            {
                foreach (var canvas in FindObjectsOfType<InkCanvas>())
                    canvas.ResetPaint();
            }
        }
    }
}
