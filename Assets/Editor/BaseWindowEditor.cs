﻿using Argentics.InkWars.Menu.UI;
using UnityEditor;

namespace Argentics.InkWars.Editor
{
    [CustomEditor(typeof(BaseWindow), true)]
    public class BaseWindowEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            var window = target as BaseWindow;
            EditorGUILayout.LabelField("ID: " + (window.ID != 0 ? window.ID.ToString() : " not set."));
            base.OnInspectorGUI();
        }
    }
}