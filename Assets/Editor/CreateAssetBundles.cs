﻿using System.IO;
using UnityEditor;
using UnityEngine;

public class CreateAssetBundles : ScriptableWizard
{

    public string versionName = "1.0";
    public BuildAssetBundleOptions buildOptions = BuildAssetBundleOptions.None;
    public BuildTarget platform = BuildTarget.Android;

    [MenuItem("Tools/Create Asset Bundle")]
    static void CreateWizard()
    {
        DisplayWizard<CreateAssetBundles>("Create Asset Bundle", "Create");
    }

    void OnWizardCreate()
    {
        string assetBundleDirectory = "Assets/AssetBundles";
        if (!Directory.Exists(assetBundleDirectory))
        {
            Directory.CreateDirectory(assetBundleDirectory);
        }

        string pathWithPlatformFolder = assetBundleDirectory + "/" + platform;

        if (!Directory.Exists(pathWithPlatformFolder))
        {
            Directory.CreateDirectory(pathWithPlatformFolder);
        }

        string pathWithVersionName = pathWithPlatformFolder + "/" + versionName;

        if (!Directory.Exists(pathWithVersionName))
        {
            Directory.CreateDirectory(pathWithVersionName);
        }

        BuildPipeline.BuildAssetBundles(pathWithVersionName, buildOptions, platform);
    }

    void OnWizardUpdate()
    {

    }

    // When the user presses the "Apply" button OnWizardOtherButton is called.
    void OnWizardOtherButton()
    {

    }
}