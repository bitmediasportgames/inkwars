﻿using Assets.Scripts;
using UnityEditor;

namespace Argentics.InkWars.Editor
{
    [CustomEditor(typeof(ApplicationManager))]
    public class ApplicationManagerDebugInfo : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            //var appManager = target as ApplicationManager;
        }
    }
}