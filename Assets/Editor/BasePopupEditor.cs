﻿using Argentics.InkWars.Menu.UI;
using UnityEditor;

namespace Argentics.InkWars.Editor
{
    [CustomEditor(typeof(BasePopup), true)]
    public class BasePopupEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            var popup = target as BasePopup;
            EditorGUILayout.LabelField("ID: " + (popup.ID != 0 ? popup.ID.ToString() : " not set."));
            base.OnInspectorGUI();
        }
    }
}